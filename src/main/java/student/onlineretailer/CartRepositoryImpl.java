package student.onlineretailer;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CartRepositoryImpl implements CartRepository{

    private Map<Integer, Integer> map = new HashMap<>();

    public void add(int itemId, int quantity) {
        if (!map.containsKey(itemId)) {
            map.put(itemId, quantity);
        } else {
            map.put(itemId, quantity + map.get(itemId));
        }
    };

    public void remove(int itemId) {
        map.remove(itemId);
    };

    public Map<Integer, Integer> getAll() {
        return map;
    };

}
