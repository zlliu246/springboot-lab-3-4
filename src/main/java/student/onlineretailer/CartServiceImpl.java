package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CartServiceImpl implements CartService{

    @Autowired
    private CartRepository repo;

    @Value("#{catalog}")
    private Map<Integer, Item> catalog;

    @Value("${contactEmail}")
    private String contactEmail;

    @Value("${onlineRetailer.salesTaxRate.value}")
    private double salesTaxRateValue;

    @Value("${onlineRetailer.salesTaxRate.description}")
    private String salesTaxRateDescription;

    public String toString() {
        return String.format("Contact email: %s\nSales tax rate value:%.2f\nSales tax rate description:%s\n",
                contactEmail, salesTaxRateValue, salesTaxRateDescription);
    }

    public void addItemToCart(int id, int quantity) {
        if (catalog.containsKey(id)) {
            repo.add(id, quantity);
        }
    }

    public void removeItemFromCart(int id) {repo.remove(id);}

    public Map<Integer, Integer> getAllItemsInCart() {return repo.getAll();}

    public double calculateCartCost() {
        System.out.println("Calculating cart cost");
        Map<Integer, Integer> map = repo.getAll();
        double out = 0;

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            int id = entry.getKey();
            int qty = entry.getValue();
            double price = catalog.get(id).getPrice();

            System.out.printf("%d %d %.3f \n", id, qty, price);
            out += qty * price;
        }

        return out;
    };

}
