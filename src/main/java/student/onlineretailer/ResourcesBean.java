package student.onlineretailer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Lazy
//@Profile("development")
public class ResourcesBean {

    @Value("${resources.db}")
    private String db;

    @Value("${resources.logs}")
    private String logs;

    @Value("${resources.secure}")
    private String secure;

    public String toString() {
        return db+ " " + logs + " " + secure;
    }

}
