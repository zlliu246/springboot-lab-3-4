package student.onlineretailer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(Application.class, args);

		CartService service = context.getBean(CartServiceImpl.class);

		service.addItemToCart(0,1);
		service.addItemToCart(1,1);
		service.addItemToCart(2,1);
		service.addItemToCart(3,1);

		System.out.println(service.getAllItemsInCart());

		System.out.printf("Total cost of stuff in the cart: %.2f \n", service.calculateCartCost());
		System.out.println("test");
		System.out.println(service.toString());

		ResourcesBean rb = context.getBean(ResourcesBean.class);
		System.out.println(rb);
	}

	@Bean
	public Map<Integer, Item> catalog() {
		Map<Integer, Item> map = new HashMap<>();
		map.put(0, new Item(0, "apple", 5));
		map.put(1, new Item(1, "orange", 6));
		map.put(2, new Item(2, "pear", 7));
		map.put(3, new Item(3, "durian", 8));

		return map;
	}

}
